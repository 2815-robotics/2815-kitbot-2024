// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkLowLevel.MotorType;

import edu.wpi.first.wpilibj.CAN;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.MotorConstants;

public class DriveSubsystem extends SubsystemBase {
  /** Creates a new DriveSubsystem. */

  private final CANSparkMax m_leftDrive1;
  private final CANSparkMax m_leftDrive2;
  private final CANSparkMax m_rightDrive1;
  private final CANSparkMax m_rightDrive2;
  private final DifferentialDrive m_drive;

  public DriveSubsystem() {
    m_leftDrive1 = new CANSparkMax(MotorConstants.kLeftMotor1Port, MotorType.kBrushless);
    m_leftDrive2 = new CANSparkMax(MotorConstants.kLeftMotor2Port, MotorType.kBrushless);
    m_rightDrive1 = new CANSparkMax(MotorConstants.kRightMotor1Port, MotorType.kBrushless);
    m_rightDrive2 = new CANSparkMax(MotorConstants.kRightMotor2Port, MotorType.kBrushless);
    m_rightDrive1.setInverted(true);
    m_rightDrive2.setInverted(true);
    m_leftDrive2.follow(m_leftDrive1);
    m_rightDrive2.follow(m_rightDrive1);
    
    m_drive = new DifferentialDrive(m_leftDrive1,m_rightDrive1);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }

  public void set(double speed, double turn){
    m_drive.arcadeDrive(speed, turn);
  }
  public void stop(){
    m_leftDrive1.set(0);
    m_rightDrive1.set(0);
    m_leftDrive2.set(0);
    m_rightDrive2.set(0);
  }
}
