package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.Constants.MotorConstants;


public class ShooterSubsytem extends SubsystemBase {
  private CANSparkMax m_shooter1;
  private CANSparkMax m_shooter2;
  private double backspeed = -.25;
  private double frontspeed = 1;
  public ShooterSubsytem(){
    m_shooter1 = new CANSparkMax(MotorConstants.kShooter1Port, MotorType.kBrushless);
    m_shooter2 = new CANSparkMax(MotorConstants.kShooter2Port, MotorType.kBrushless);
    m_shooter1.setIdleMode(IdleMode.kBrake);
    m_shooter2.setIdleMode(IdleMode.kBrake);
    m_shooter1.burnFlash();
    m_shooter2.burnFlash();
    m_shooter2.follow(m_shooter1);
  }

  public void spinfront() {
      m_shooter1.set(frontspeed);
  }

  public void spinback() {
      m_shooter1.set(backspeed);
  }

  public void notspin() {
      m_shooter1.set(0);
  }

  private static ShooterSubsytem m_Instance;
  public static ShooterSubsytem getInstance() {
      if (m_Instance == null) {
          m_Instance = new ShooterSubsytem();
      }
      return m_Instance;
  }
}
