package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.PneumaticHub;
import edu.wpi.first.wpilibj.PneumaticsControlModule;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class TrapSubsytem extends SubsystemBase{
    private final DoubleSolenoid mSolenoid;
    private final DoubleSolenoid mSolenoid2;
    private final Compressor m_compressor;
    private final PneumaticHub m_hub;
    public TrapSubsytem() {
        mSolenoid = new DoubleSolenoid(PneumaticsModuleType.REVPH, 12, 13);
        mSolenoid2 = new DoubleSolenoid(PneumaticsModuleType.REVPH, 14, 15);
        m_compressor = new Compressor(PneumaticsModuleType.REVPH);
        m_hub = new PneumaticHub(8);
        mSolenoid.set(Value.kReverse);
        mSolenoid2.set(Value.kReverse);
    }

    public void toggleSolenoid() {
        mSolenoid.toggle();
        mSolenoid2.toggle();
    }

    public void setSolenoid2() {
        mSolenoid.set(Value.kForward);
        mSolenoid2.set(Value.kForward);

    }


    public Command runToggleSolenoid() {
        return new InstantCommand(() -> this.toggleSolenoid());
    }

    public Command runsetSolenoid() {
        return new InstantCommand(() -> this.setSolenoid2());
    }

    @Override
    public void periodic() {
        if (mSolenoid.get() == Value.kForward) {
            SmartDashboard.putBoolean("solenoid", true);
        } else {
            SmartDashboard.putBoolean("solenoid", false);
        }
        if (mSolenoid2.get() == Value.kForward) {
            SmartDashboard.putBoolean("solenoid2", true);
        } else {
            SmartDashboard.putBoolean("solenoid2", false);
        }
    }
}
