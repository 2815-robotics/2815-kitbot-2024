// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkBase;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkLowLevel.MotorType;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.MotorConstants;

public class AmpSubsystem extends SubsystemBase {
  /** Creates a new AmpSubsystem. */
  private CANSparkMax m_ampMotor;
  private double speed = 1;
  

  public AmpSubsystem() {
    m_ampMotor = new CANSparkMax(MotorConstants.kampPort , MotorType.kBrushless);
    m_ampMotor.setIdleMode(IdleMode.kBrake);
    m_ampMotor.burnFlash();
  }

  public void spinfront(){
    m_ampMotor.set(speed);
  }

  public void spinback(){
    m_ampMotor.set(-speed);
  }

  public void notspin(){
    m_ampMotor.set(0);
  }

  private static AmpSubsystem m_Instance;
  public static AmpSubsystem getInstance() {
      if (m_Instance == null) {
          m_Instance = new AmpSubsystem();
      }
      return m_Instance;
  }


  @Override
  public void periodic() {
    // This method will be called once per scheduler run
        SmartDashboard.putNumber("amp", m_ampMotor.get());
  }
}
