package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.RunCommand;
import frc.robot.subsystems.AmpSubsystem;

public class AmpCommand {
    private static final AmpSubsystem m_ampSubsystem = AmpSubsystem.getInstance();

    public static Command spinfront() {
        return new RunCommand(
            () -> m_ampSubsystem.spinfront()
        );
    }
    public static Command spinback() {
        return new RunCommand(
            () -> m_ampSubsystem.spinback()
        );
    }
    public static Command stopspin() {
        return new RunCommand(
            () -> m_ampSubsystem.notspin()
        );
    }
    
}
