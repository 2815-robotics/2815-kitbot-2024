package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.RunCommand;
import frc.robot.subsystems.ShooterSubsytem;
public class ShooterCommand {
    private static final ShooterSubsytem m_intake = ShooterSubsytem.getInstance();

    public static Command spinfront() {
        return new RunCommand(
            () -> m_intake.spinfront()
        );
    }
    public static Command spinback() {
        return new RunCommand(
            () -> m_intake.spinback()
        );
    }
    public static Command stopspin() {
        return new RunCommand(
            () -> m_intake.notspin()
        );
    }
    
}
