// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import frc.robot.subsystems.DriveSubsystem;

public class DriveCommand extends Command {

  private DriveSubsystem m_DriveSubsystem;
  private CommandXboxController m_DriverController;

  private double left;
  private double right;

  SlewRateLimiter left_Limiter = new SlewRateLimiter(10);
  SlewRateLimiter right_Limiter = new SlewRateLimiter(10);

  /** Creates a new DriveCommand. */
  public DriveCommand(DriveSubsystem m_DriveSubsystem, CommandXboxController m_DriverController) {
    // Use addRequirements() here to declare subsystem dependencies.

    this.m_DriveSubsystem = m_DriveSubsystem;
    this.m_DriverController = m_DriverController;

    addRequirements(m_DriveSubsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    left = 0;
    right = 0;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    left = MathUtil.applyDeadband(m_DriverController.getRightX(), 0.1);
    right = MathUtil.applyDeadband(m_DriverController.getLeftY(), 0.1);

    m_DriveSubsystem.set(-left_Limiter.calculate(left),-right_Limiter.calculate(right));

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    m_DriveSubsystem.stop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
